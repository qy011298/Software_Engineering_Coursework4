**Introduction**

The system being created is a browser based game for university students to use as a revision tool. The game will centre around solving puzzles in a graphical adventure style, using knowledge gained through reading historical sources to solve them.

The software is being developed with the intention of causing the university students to be more proactive in revision and reading their required case studies.

This document will consider important aspects of the project including an estimation of the resources and subsequent costs of the project, the social and legal considerations and the development methodology that will be used. This document serves as an insight into the process that'd be taken to develop this software.

**Social and Legal Aspects**

Throughout the development of software, certain considerations must be taken to ensure the product being made is legally and socially acceptable. Though this may not necessarily affect the quality of the final product, it can greatly differ the reception it receives on release.

An example of a legal constraint for the project is the requirement that the game must comply to the Data Protection Act 1998 [https://www.legislation.gov.uk/ukpga/1998/29/contents]. The act states that reasonable steps must be made to protect and hide data, and that the data must be relevant and up to date.
As the software will be run on a server held by the university, with transmission of web pages through a separate web-server, the game will need to transmit data between the client computer and the server. During this process, there is a risk of data such as usernames and passwords, being intercepted which could lead to further personal information being found. To minimise this risk and comply with the act, this data transmission could be encrypted. In addition to this, functionality must be added so the information of the student can be easily deleted from the system in the case of they leave or graduate.

Another legal constraint is the need to secure rights to the use of historical sources, or information provided by the student's textbooks. If the information is used but the rights aren't attained, this could be copyright infringement under the Copyright, Designs and Patent act 1988 [https://www.copyrightservice.co.uk/copyright/uk_law_summary]. 
As this game is to be developed for a non-commercial use and to be used for education, much of the information may fall under "fair use", which is the ability to use portions of copyright material without permission [https://fairuse.stanford.edu/overview/academic-and-educational-permissions/non-coursepack/]. If the information required for the game does not fall under fair use, permission can often be obtained through request from the copyright owner, which potentially includes an agreed upon fee. 

An example of a social aspect that will be taken into consideration through development, is the level of censorship required. This could be in the case of potentially offensive language, or imagery. For instance, a description of the game [Software Engineering Coursework Brief 1] states that the student's curriculum includes the history of the Atlantic and the slave trade. Some information within these sections of the textbooks or language may be offensive to the students and so censorship may be required. Though this is important to ensure the user enjoys the product, the practicality of using it will be greatly diminished if the information is too censored. Research could be conducted to determine the correct level of censorship required.

**Choice of Development Methodology and Justification**.

The way the software is developed is an important factor when considering most aspects about the project, such as resource cost, estimated time to develop, and the development process itself.

The two main ways of developing is using the plan based approach, or the agile development method. Plan based development tends to be used in larger projects, and consists of meticulously planning the project before development. Agile is much more past paced and inter-leaves the specification, design and implementation of it into one.

The way this project will be developed is through the use of Agile development, or more specifically the Scrum method. As the project is reasonably small, and the client is closely involved in the development of it, Agile will allow for a better project to be made with a lot less overheads for the company or the client. 

The client is able to outline specific wants and needs intitially which will then be followed by a series of sprint cycles. These cycles will develop a new feature or component for the software, where this can then be reviewed by the client. This ensures that the software will constantly be developed to the client's requirements, even if they're changing, resulting in a better overall quality.

This method of working ensures the turn around time of the programme will be much lower than a piece of software developed using the plan-based methodology and consequently, a much lower cost of resources. The trade off to have this is that an as accurate quote to the client can't be given when using Agile, and a lack of risk mitigation in the event of a problem. However, the likelihood of requirements changing throughout the development of the project is high and the agile method handles this eventuality much better than plan-based, which would result in the plan quickly going out of date.

It can be estimated that the optimal size of an agile development team is 7 (plus or minus 2) [http://rgalen.com/agile-training-news/2015/8/22/the-3-bears-of-agile-team-size], not including the ScrumMaster. When using agile methodology for development, the design process can be carried out in a similar way. Though, as designs can be made for a feature faster than a developer can code it [https://uxstudioteam.com/ux-blog/agile-design-process/] there are less designers than developers in the project. The scrum methodology will be used along with test-driven development, in which automated unit tests and component tests are carried out throughout development, that the developers will write themselves. This mitigates the need for testers, further reducing the costs of the project.

**Software Architecture**

The system architecture is concerned with how each component of both the software and hardware components will connect to build the whole programme. Factors that affect this will be the current hardware and software available to the client currently, as well as the structure and components of the proposed programme. 
The benefit of using these is that is can provide a very basic structure for the programme being developed as well as act as a good communication tool between the developers and the stakeholders. [Ch6_Architectural_design__1_.pptx](/uploads/757fe3b23fb2c534d95eef99795f8153/Ch6_Architectural_design__1_.pptx)

The software architecture is based mostly on the specification of the client's current hardware, as well as the requirements of the proposed software. The client currently has a web server that can act as the application server as well as the web server, in addition to a separate account management server and database server.
The current hardware available to the client best suits a distributed client-server architecture in which the central server will host the game and handle the data processing [https://www.utdallas.edu/~chung/SA/2client.pdf] with the web server delivering the pages across the universities intranet. The database server will then be able to store the player's high scores, and the account management system will handle the login requirement of the game.
This architecture allows the client to add additional computers to the system easily and without the need of storing any files locally on each computer. This also allows for the client computers to have a less processing power as the server will be conducting the main functionality, and distributing it to the client computer.
The disadvantage to this architecture is that there is a single point of failure for the system, more specifically the server. If it fails, the software won't be accessible by any client computer. There is also requirement for a strong network connection to ensure the server and client's can communicate efficiently and the software runs smoothly. [Ch6_Architectural_design__1_.pptx](/uploads/757fe3b23fb2c534d95eef99795f8153/Ch6_Architectural_design__1_.pptx)

![ProposalDiagram](/uploads/f60f433c8216327b2a755f6845674ccf/ProposalDiagram.png)

The above is a diagram of the proposed architecture. The university's web server is being used to host the application as well as responsible with communicating with each of the client's web browsers. As they're the same server, they're connected in the diagram but they are considered different as they have different processes. The application server is responsible for implementing the application-specific logic, and retrieval requests [ch6, slide 51], while the web server is tasked with sending this information from the application server to the client's machines.

If the users of the client computers want to store high scores, the application server is able to communicate with the universities database server to do this. The application server is also connected to the existing Account Management Server that the university already uses to handle the user accounts. 

**Programming Languages: [Oliver Barnwell Private Conversation]**

The following is a list of the programming languages that would be required throughout the development of the project, with justification for using it.

HTML + CSS - These will be used to develop the web pages that are delivered from the web server to the client computers. This was found from the Client's requirements. [brief]

JavaScript (JS)- This will be embedded into the web pages and gives the functionality and scripting to the game. This was found from the Client's requirements. [brief] This can also be used for the server side scripting language. There should be separate programmers that know JS for front end and back end. This is because JS is known to be a very powerful tool for server side scripting, however it is a more difficult language to learn. [https://www.sitepoint.com/server-side-language-right/] 

SQL- This will be used as a query language for the database server, that will return the scores of certain players. As the databases will be relational due to the high scores being linked to the player, this would be the most common language to use, and one most programmers should know.

**Estimation of Resources, Costs and What to Charge the Client**

Referring to the development methodology section, the required team will be 5-9 in size (not including Scrum-Master), with developers outnumbering designers. As TDD is being used to develop the software, there is no requirement for testers as the developers are expected to carry out their own testing. 

The resources are as follows:

2 Experienced Programmers. One will be in charge of the front end, whilst one is used for the back end development. These will be also able to support the inexperienced programmers or gain support from them, when required. The back-end programmer is required to know JavaScript and SQL, whilst the front end programmer will be required to know HTML and CSS.

3 Inexperienced Programmers. These can be assigned to the back end or front end depending on which experienced programmer requires more support. 2 programmers can also double up to carry out pair programming, to ensure less mistakes are made in development of the programme. 2 of these programmers will know JavaScript and one will know CSS to ensure both the front end and back end developers are supported.

2 Narrative Designers- These will be used to create the story lines that are required from the client. Assuming that a story line takes 4 months for a single person to create, the required 3 can be implemented into the software in the required time frame.


In addition to this team, a Scrum-Master will be required to oversee the progress of the development, and ensure the project will be completed in the required time frame.

Referring to [brief], the estimated cost to the company, over the 6 months will be as follows:


2 x £100,000 x 0.5 = £100,000. For the experienced programmers.

3 x £56,000 x 0.5 = £84,000. For the inexperienced programmers.

2 x £50,000 x 0.5 = £50,000. For the narrative designers.

1 x £62,500 x 0.5 = £31,250. For the scrum master. [https://www.cwjobs.co.uk/salary-checker/average-scrum-master-salary]

8 x £250 x 6 = £12,000. For the office space of 8 people over a 6 month period. [brief]


Total = £277,250.

These are according to the following assumptions :


The project is able to be finished within the 6 month period.

The staff are working solidly over the 6 month period, in office.

The 2 Narrative Designers are able to complete 3 story lines and have them implemented in the 6 month period.

The information that's required in the software falls under Fair-Use and no royalties are required to be paid.

Taking into account a ~30% net profit margin [https://www.thefastlaneforum.com/community/threads/what-should-my-gross-margin-be-for-a-custom-software-company.63968/] , the total amount charged to the client should be in the region of £360,000 for this piece of software.

**Module Reflection**

Throughout the module we've learned about the processes that many software engineers will go through to develop a piece of software that has been commissioned from a client. This includes aspects such as the process of determining the complete requirements from the client, and building a product specification that is usable; The various development methodologies and the situations in which they are best used; The social and legal considerations and soft skills that support these aspects, such as essay writing, working in groups, researching and siting sources correctly.

We learned the theory behind each process through lectures, which would then be used in practical application in the course works, of which we did group and individual ones. These served as a way of cementing the theory whilst teaching us various ways we would be working in industry.

I found the course to be enjoyable and interesting. The expectation to research independently as well as use materials given to us, causes much more proactive learning in the module, which will benefit when put into practice. The addition of soft skills being taught to us will benefit the whole degree beyond this module and make us start good habits early.

**Sources**